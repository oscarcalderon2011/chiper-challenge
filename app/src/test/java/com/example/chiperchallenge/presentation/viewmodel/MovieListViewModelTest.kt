package com.example.chiperchallenge.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.chiperchallenge.domain.actions.GetMovies
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.factory.DummyMovies.FORREST_GUMP
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.example.chiperchallenge.factory.DummyMovies.TITANIC
import com.example.chiperchallenge.presentation.viewmodel.MovieListViewModel.Status
import com.example.chiperchallenge.rules.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test

class MovieListViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Rule
    @JvmField var testSchedulerRule = RxImmediateSchedulerRule()

    private val getMovies = mock<GetMovies> {  }
    private lateinit var viewModel: MovieListViewModel

    @Test
    fun `update movies`() {
        givenMovies(listOf(SUPERMAN, TITANIC, FORREST_GUMP))

        whenCreatingViewModel()

        then(viewModel.movies.value)
                .containsExactlyElementsOf(listOf(SUPERMAN, TITANIC, FORREST_GUMP))
    }

    @Test
    fun `updating in-progress state`() {
        whenever(getMovies.invoke()).thenReturn(Single.never())

        whenCreatingViewModel()

        then(viewModel.status.value).isEqualTo(Status.IN_PROGRESS)
    }

    @Test
    fun `updating finished state`() {
        givenMovies(listOf(SUPERMAN, TITANIC, FORREST_GUMP))

        whenCreatingViewModel()

        then(viewModel.status.value).isEqualTo(Status.FINISHED)
    }

    private fun whenCreatingViewModel() {
        viewModel = MovieListViewModel(getMovies)
    }

    private fun givenMovies(movies: List<Movie>) {
        whenever(getMovies.invoke()).thenReturn(Single.just(movies))
    }
}