package com.example.chiperchallenge.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.chiperchallenge.domain.actions.GetMovieDetail
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.example.chiperchallenge.rules.RxImmediateSchedulerRule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Maybe
import org.assertj.core.api.BDDAssertions.then
import org.junit.Rule
import org.junit.Test

class MovieDetailViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Rule
    @JvmField var testSchedulerRule = RxImmediateSchedulerRule()

    private val getMovieDetail = mock<GetMovieDetail> {  }
    private val viewModel: MovieDetailViewModel = MovieDetailViewModel(getMovieDetail)

    @Test
    fun `get movie detail`() {
        givenMovieDetail(SUPERMAN.id, SUPERMAN)

        viewModel.findMovieBy(SUPERMAN.id)

        then(viewModel.movie.value).isEqualTo(SUPERMAN)
    }

    private fun givenMovieDetail(id: Long, movie: Movie) {
        whenever(getMovieDetail.invoke(id)).thenReturn(Maybe.just(movie))
    }
}