package com.example.chiperchallenge.infrastructure.repository

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MovieId
import com.example.chiperchallenge.factory.DummyMovies.FORREST_GUMP
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.example.chiperchallenge.factory.DummyMovies.TITANIC
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test
import org.mockito.Mockito

class CachedMoviesRepositoryTest {

    private val diskRepository = mock<DiskMoviesRepository> {  }
    private val apiRepository = mock<ApiMoviesRepository> {  }

    private val cachedRepository = CachedMoviesRepository(diskRepository, apiRepository)
    private var expectedMovies = TestObserver<List<Movie>>()
    private var expectedMovie = TestObserver<Movie>()

    @Test
    fun `retrieve movies from api`() {
        givenAnEmptyCache()
        givenMoviesFromApi(listOf(
            SUPERMAN,
            TITANIC
        ))

        whenMoviesAreRetrieved()

        thenExpectedMoviesAre(listOf(
            SUPERMAN,
            TITANIC
        ))
    }

    @Test
    fun `retrieve movies from disk`() {
        givenMoviesFromApi(listOf(FORREST_GUMP))
        givenMoviesFromDisk(listOf(
            SUPERMAN,
            TITANIC
        ))

        whenMoviesAreRetrieved()

        thenExpectedMoviesAre(listOf(
            SUPERMAN,
            TITANIC
        ))
    }

    @Test
    fun `popular movies should be stored in cache`() {
        givenAnEmptyCache()
        givenMoviesFromApi(listOf(SUPERMAN, TITANIC))

        whenMoviesAreRetrieved()

        Mockito.verify(diskRepository).addAll(listOf(SUPERMAN, TITANIC))
    }

    @Test
    fun `retrieve movie by id from disk`() {
        givenMovieById(TITANIC.id, TITANIC)

        whenMovieAreRetrievedById(TITANIC.id)

        thenExpectedMovieIs(TITANIC)
    }

    private fun givenAnEmptyCache() {
        Mockito.`when`(diskRepository.findAll()).thenReturn(Single.just(emptyList()))
    }

    private fun givenMoviesFromApi(movies: List<Movie>) {
        Mockito.`when`(apiRepository.findAll()).thenReturn(Single.just(movies))
    }

    private fun givenMoviesFromDisk(movies: List<Movie>) {
        Mockito.`when`(diskRepository.findAll()).thenReturn(Single.just(movies))
    }

    private fun givenMovieById(movieId: MovieId, movie: Movie) {
        Mockito.`when`(diskRepository.findById(movieId)).thenReturn(Maybe.just(movie))
    }

    private fun whenMoviesAreRetrieved() {
        expectedMovies = cachedRepository.findAll().test()
    }

    private fun whenMovieAreRetrievedById(movieId: MovieId) {
        expectedMovie = cachedRepository.findById(movieId).test()
    }

    private fun thenExpectedMoviesAre(expectedMovies: List<Movie>) {
        then(this.expectedMovies.values()[0]).containsExactlyElementsOf(expectedMovies)
    }
    private fun thenExpectedMovieIs(movie: Movie) {
        then(this.expectedMovie.values()[0]).isEqualTo(movie)
    }
}