package com.example.chiperchallenge.infrastructure

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.factory.DummyMovies.FORREST_GUMP
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.example.chiperchallenge.factory.DummyMovies.TITANIC
import com.example.chiperchallenge.infrastructure.repository.DiskMoviesRepository
import com.example.chiperchallenge.infrastructure.storage.LocalStorage
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test
import org.mockito.Mockito.*

class DiskMoviesRepositoryTest {

    private val localStorage = mock<LocalStorage<Movie>> {  }
    private val diskMoviesRepository = DiskMoviesRepository(localStorage)
    private var expectedMovies = TestObserver<List<Movie>>()
    private var movie = TestObserver<Movie>()

    @Test
    fun `retrieve all movies`() {
        givenSavedMovies(listOf(
            SUPERMAN,
            TITANIC,
            FORREST_GUMP
        ))

        whenAllMoviesAreRetrieved()

        thenExpectedMoviesAre(listOf(
            SUPERMAN,
            TITANIC,
            FORREST_GUMP
        ))
    }

    @Test
    fun `retrieve movie by id`() {
        val supermanId = 1001L
        givenSavedMovies(listOf(
            SUPERMAN,
            TITANIC,
            FORREST_GUMP
        ))

        whenFindingMovieById(supermanId)

        movie.assertValue { it == SUPERMAN }
    }

    private fun whenFindingMovieById(supermanId: Long) {
        movie = diskMoviesRepository.findById(supermanId).test()
    }

    private fun givenSavedMovies(movies: List<Movie>) {
        `when`(localStorage.finAll()).thenReturn(movies)
    }

    private fun whenAllMoviesAreRetrieved() {
        expectedMovies = diskMoviesRepository.findAll().test()
    }

    private fun thenExpectedMoviesAre(expectedMovies: List<Movie>) {
        then(this.expectedMovies.values()[0]).containsExactlyElementsOf(expectedMovies)
    }
}