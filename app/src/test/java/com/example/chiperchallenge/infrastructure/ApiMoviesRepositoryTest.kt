package com.example.chiperchallenge.infrastructure

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.factory.DummyMovies.FORREST_GUMP
import com.example.chiperchallenge.factory.DummyMovies.FORREST_GUMP_RESPONSE
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN_RESPONSE
import com.example.chiperchallenge.factory.DummyMovies.TITANIC
import com.example.chiperchallenge.factory.DummyMovies.TITANIC_RESPONSE
import com.example.chiperchallenge.infrastructure.client.TmdbClient
import com.example.chiperchallenge.infrastructure.repository.ApiMoviesRepository
import com.example.chiperchallenge.infrastructure.representation.MovieResponse
import com.example.chiperchallenge.infrastructure.representation.MoviesResponse
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import org.assertj.core.api.BDDAssertions.then
import org.junit.Test

class ApiMoviesRepositoryTest {

    private val moviesClient = mock<TmdbClient> {  }
    private val apiRepository = ApiMoviesRepository(moviesClient)
    private var expectedMovies = TestObserver<List<Movie>>()

    @Test
    fun `retrieve all movies`() {
        givenPopularMoviesFromApi(listOf(
            SUPERMAN_RESPONSE,
            FORREST_GUMP_RESPONSE,
            TITANIC_RESPONSE
        ))

        whenPopularMoviesAreRetrieved()

        thenExpectedMoviesAre(listOf(
            SUPERMAN,
            FORREST_GUMP,
            TITANIC
        ))
    }

    private fun givenPopularMoviesFromApi(movies: List<MovieResponse>) {
        whenever(moviesClient.findPopularMovies()).thenReturn(Single.just(MoviesResponse(movies)))
    }

    private fun whenPopularMoviesAreRetrieved() {
        expectedMovies = apiRepository.findAll().test()
    }

    private fun thenExpectedMoviesAre(movies: List<Movie>) {
        then(this.expectedMovies.values()[0]).containsExactlyElementsOf(movies)
    }
}