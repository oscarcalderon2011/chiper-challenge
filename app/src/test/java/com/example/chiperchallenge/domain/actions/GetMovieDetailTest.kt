package com.example.chiperchallenge.domain.actions

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MoviesRepository
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Maybe
import io.reactivex.observers.TestObserver
import org.junit.Test

class GetMovieDetailTest {

    private val movieRepository = mock<MoviesRepository> {  }
    private val getMovieDetail = GetMovieDetail(movieRepository)
    private var movie = TestObserver<Movie>()

    @Test
    fun `get movie detail`() {
        givenMovie(3L, SUPERMAN)

        movie = getMovieDetail.invoke(3L).test()

        movie.assertValue { it == SUPERMAN }
    }

    private fun givenMovie(movieId: Long, movie: Movie) {
        whenever(movieRepository.findById(movieId)).thenReturn(Maybe.just(movie))
    }
}