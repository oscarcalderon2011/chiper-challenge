package com.example.chiperchallenge.domain.actions

import com.example.chiperchallenge.factory.DummyMovies.FORREST_GUMP
import com.example.chiperchallenge.factory.DummyMovies.SUPERMAN
import com.example.chiperchallenge.factory.DummyMovies.TITANIC
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MoviesRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import junit.framework.TestCase


class GetMoviesTest : TestCase() {

    private val moviesRepository = mock<MoviesRepository>{ }
    private val getPopularMovies  = GetMovies(moviesRepository)
    private var expectedMovies = TestObserver<List<Movie>>()

    fun `test retrieve movies`() {
        val movies = listOf(
            SUPERMAN,
            FORREST_GUMP,
            TITANIC
        )
        givenMovies(movies)

        whenGettingMovies()

        thenFoundMoviesAre(movies)
    }

    private fun givenMovies(movies: List<Movie>) {
        whenever(moviesRepository.findAll()).thenReturn(
            Single.just(
                movies
            )
        )
    }

    private fun thenFoundMoviesAre(movies: List<Movie>) {
        expectedMovies.assertValue { it.containsAll(movies) }
    }

    private fun whenGettingMovies() {
        expectedMovies = getPopularMovies().test()
    }

}

