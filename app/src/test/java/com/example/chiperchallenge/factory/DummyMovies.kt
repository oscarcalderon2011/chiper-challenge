package com.example.chiperchallenge.factory

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.infrastructure.representation.MovieResponse

private const val thumbnailUrl = "https://image.tmdb.org/t/p/w200"
private const val posterUrl = "https://image.tmdb.org/t/p/w300"
const val releaseDate = "01/10/2020"
const val dummyOverView = "film description"

object DummyMovies {
    val SUPERMAN = Movie(
        1001L,
        "superman",
        thumbnail = "${thumbnailUrl}superman-thumb",
        poster = "${posterUrl}superman-poster",
        releaseDate = releaseDate,
        overview = dummyOverView
    )
    val FORREST_GUMP = Movie(
        1003L,
        "forrest gump",
        thumbnail = "${thumbnailUrl}forrest-thumb",
        poster = "${posterUrl}forrest-poster",
        releaseDate = releaseDate,
        overview = dummyOverView
    )
    val TITANIC = Movie(
        1004L,
        "titanic",
        thumbnail = "${thumbnailUrl}titanic-thumb",
        poster = "${posterUrl}titanic-poster",
        releaseDate = releaseDate,
        overview = dummyOverView
    )

    val SUPERMAN_RESPONSE = MovieResponse(
        1001L,
        "superman",
        thumbnail = "superman-thumb",
        poster = "superman-poster",
        releaseDate = releaseDate,
        overview = dummyOverView,
    )
    val FORREST_GUMP_RESPONSE = MovieResponse(
        1003L,
        "forrest gump",
        thumbnail = "forrest-thumb",
        poster = "forrest-poster",
        releaseDate = releaseDate,
        overview = dummyOverView
    )
    val TITANIC_RESPONSE = MovieResponse(
        1004L,
        "titanic",
        thumbnail = "titanic-thumb",
        poster = "titanic-poster",
        releaseDate = releaseDate,
        overview = dummyOverView
    )


}