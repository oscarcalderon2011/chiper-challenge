package com.example.chiperchallenge.di

import android.content.Context
import com.example.chiperchallenge.domain.actions.GetMovieDetail
import com.example.chiperchallenge.domain.actions.GetMovies
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.infrastructure.repository.ApiMoviesRepository
import com.example.chiperchallenge.infrastructure.repository.CachedMoviesRepository
import com.example.chiperchallenge.infrastructure.repository.DiskMoviesRepository
import com.example.chiperchallenge.infrastructure.storage.SharedPreferencesStorage
import com.google.gson.Gson

object ActionsFactory {

    fun createGetMovies(context: Context): GetMovies {
        return GetMovies(createApiMoviesRepository(context))
    }

    fun createGetMovieDetail(context: Context): GetMovieDetail {
        return GetMovieDetail(createApiMoviesRepository(context))
    }

    private fun createApiMoviesRepository(context: Context): CachedMoviesRepository {
        val sharedPreferences = context.getSharedPreferences(MOVIE_APP_KEY, Context.MODE_PRIVATE)
        val storage = SharedPreferencesStorage<Movie>(sharedPreferences, Gson())
        val apiRepository = ApiMoviesRepository(RetrofitClientFactory.createMoviesClient())
        val diskRepository = DiskMoviesRepository(storage)
        return CachedMoviesRepository(diskRepository, apiRepository)
    }

    private const val MOVIE_APP_KEY = "movie_app_key"
}