package com.example.chiperchallenge.di

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.chiperchallenge.di.ActionsFactory.createGetMovies
import com.example.chiperchallenge.presentation.viewmodel.MovieDetailViewModel
import com.example.chiperchallenge.presentation.viewmodel.MovieListViewModel

object ViewModelFactory {

    fun provideListViewModel(fragment: Fragment, context: Context): MovieListViewModel {
        return ViewModelProviders.of(
            fragment,
            createViewModelFactory(
                MovieListViewModel(createGetMovies(context))
            )).get(MovieListViewModel::class.java)
    }

    private fun <T>createViewModelFactory(viewModel: T): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return viewModel as T
            }
        }
    }

    fun createMovieDetailViewModel(fragment: Fragment, context: Context): MovieDetailViewModel {
        return ViewModelProviders.of(
            fragment,
            createViewModelFactory(
                MovieDetailViewModel(ActionsFactory.createGetMovieDetail(context))
            )).get(MovieDetailViewModel::class.java)
    }
}