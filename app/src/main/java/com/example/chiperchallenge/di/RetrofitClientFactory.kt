package com.example.chiperchallenge.di

import com.example.chiperchallenge.infrastructure.client.TmdbClient
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClientFactory {

    private val authInterceptor = Interceptor {chain->
        val newUrl = chain.request().url
            .newBuilder()
            .addQueryParameter("api_key", "dbbdadfa5fafffc08d9295c406fe02da")
            .build()

        val newRequest = chain.request()
            .newBuilder()
            .header("Content-Type", "application/json")
            .url(newUrl)
            .build()

        chain.proceed(newRequest)
    }

    private fun retrofit() : Retrofit = Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(tmdbClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    private val tmdbClient = OkHttpClient().newBuilder()
        .addInterceptor(authInterceptor)
        .build()

    fun createMoviesClient(): TmdbClient {
        return retrofit().create(TmdbClient::class.java)
    }
}