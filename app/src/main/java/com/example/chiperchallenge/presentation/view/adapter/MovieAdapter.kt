package com.example.chiperchallenge.presentation.view.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.chiperchallenge.R
import com.example.chiperchallenge.domain.entity.Movie
import com.squareup.picasso.Picasso

class MovieAdapter(private val movies: List<Movie>, private var movieItemListener: OnMovieSelectedListener) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var movie : Movie? = null
        private var name: TextView = view.findViewById(R.id.lbl_title)
        private var thumbnail: ImageView = view.findViewById(R.id.img_thumbnail_movie)
        private var popularity: TextView = view.findViewById(R.id.lbl_popularity)
        private var voteAverage: TextView = view.findViewById(R.id.lbl_vote_average)
        private val picasso = Picasso.get()

        init {
            view.setOnClickListener(this)
            picasso.setIndicatorsEnabled(true)
        }

        override fun onClick(view: View?) {
            movie?.let { movieItemListener.onMovieSelected(it) }
        }

        fun bindMovie(movie: Movie) {
            this.movie = movie
            name.text = movie.name
            popularity.text = "Popularity ${movie.popularity} "
            voteAverage.text = "Avg ${movie.voteAverage}"
            loadImage(movie.thumbnail)
        }

        private fun loadImage(image: String) {
            picasso.load(image)
                .error(getFallbackDrawable())
                .into(thumbnail)

        }

        private fun getFallbackDrawable(): Drawable {
            return ContextCompat.getDrawable(view.context, R.drawable.ic_local_movies_24dp)!!
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.movie_item_view, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        holder.bindMovie(movie)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    interface OnMovieSelectedListener {
        fun onMovieSelected(movie: Movie)
    }
}