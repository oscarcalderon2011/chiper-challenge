package com.example.chiperchallenge.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.chiperchallenge.domain.actions.GetMovieDetail
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.extensions.defaultSchedulers
import io.reactivex.disposables.CompositeDisposable

class MovieDetailViewModel(private val getMovieDetail: GetMovieDetail) : ViewModel() {

    private val subscriptions = CompositeDisposable()
    private val mutableMovie: MutableLiveData<Movie?> = MutableLiveData()
    val movie: LiveData<Movie?> = mutableMovie

    fun findMovieBy(id: Long){
            getMovieDetail(id)
                .defaultSchedulers()
                .subscribe(::onSuccess, ::onError)
                .apply { subscriptions.add(this) }
    }

    private fun onSuccess(result: Movie?) {
        mutableMovie.value = result
    }

    private fun onError(throwable: Throwable) {
        throwable.message?.let {
            Log.e("movie error:", it)
        }
    }

    override fun onCleared() {
        subscriptions.clear()
    }
}
