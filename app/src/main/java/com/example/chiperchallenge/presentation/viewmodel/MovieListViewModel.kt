package com.example.chiperchallenge.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.chiperchallenge.domain.actions.GetMovies
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.extensions.defaultSchedulers
import io.reactivex.disposables.CompositeDisposable

class MovieListViewModel(getMovies: GetMovies) : ViewModel() {

    private val subscriptions = CompositeDisposable()
    private val mutableMovies = MutableLiveData<List<Movie>>()
    val movies: LiveData<List<Movie>> = mutableMovies
    private val mutableStatus = MutableLiveData<Status>()
    val status: LiveData<Status> = mutableStatus

    init {
        getMovies()
                .defaultSchedulers()
                .doFinally { hideProgressIndicator() }
                .doOnSubscribe { showProgressIndicator() }
                .subscribe(
                    { onSucceededRequest(it) },
                    { onFailedRequest(it) })
                .apply { subscriptions.add(this) }
    }


    private fun hideProgressIndicator() {
        mutableStatus.value = Status.FINISHED
    }

    private fun showProgressIndicator() {
        mutableStatus.value = Status.IN_PROGRESS
    }

    private fun onSucceededRequest(movies: List<Movie>) {
        mutableMovies.value = movies
    }

    private fun onFailedRequest(throwable: Throwable) {
        throwable.message?.let {
            Log.e("movies", it)
        }
    }

    enum class Status {
        IN_PROGRESS,
        FINISHED
    }

}