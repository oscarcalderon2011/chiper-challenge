package com.example.chiperchallenge.presentation.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.example.chiperchallenge.R
import com.example.chiperchallenge.di.ViewModelFactory
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.extensions.onChange
import com.example.chiperchallenge.extensions.replaceFragment
import com.example.chiperchallenge.presentation.viewmodel.MovieListViewModel
import com.example.chiperchallenge.presentation.viewmodel.MovieListViewModel.Status.IN_PROGRESS
import com.example.chiperchallenge.presentation.view.adapter.MovieAdapter
import kotlinx.android.synthetic.main.fragment_movie_list.*


class MovieListFragment : Fragment(R.layout.fragment_movie_list) {

    private lateinit var movieAdapter: MovieAdapter
    private lateinit var movieListViewModel: MovieListViewModel
    private lateinit var progressIndicator : MaterialDialog
    private val movies = mutableListOf<Movie>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        setupProgressIndicator()
        initViewModel()
    }

    private fun initViewModel() {
        movieListViewModel = ViewModelFactory.provideListViewModel(this, requireContext())
        onChange(movieListViewModel.movies) {
            updateMovies(it)
        }

        onChange(movieListViewModel.status) {
            updateStatus(it)
        }
    }

    private fun setupProgressIndicator() {
        context?.let {
            progressIndicator = MaterialDialog.Builder(it)
                .content("loading ...")
                .progress(true, 0)
                .build()
        }
    }

    private fun updateStatus(status: MovieListViewModel.Status?) {
        status?.let {
            when (status) {
                IN_PROGRESS -> showProgressIndicator()
                else -> dismissProgressIndicator()
            }
        }
    }

    private fun updateMovies(updatedMovies: List<Movie>) {
            movies.clear()
            movies.addAll(updatedMovies)
            movieAdapter.notifyDataSetChanged()
    }


    private fun initRecycler() {
        movieAdapter = MovieAdapter(movies, createMovieListener())
        movie_list.layoutManager = LinearLayoutManager(context)
        movie_list.adapter = movieAdapter
    }

    private fun createMovieListener(): MovieAdapter.OnMovieSelectedListener {
        return object : MovieAdapter.OnMovieSelectedListener {
            override fun onMovieSelected(movie: Movie) {
                navigateToMovieDetail(movie.id)
            }
        }
    }

    private fun showProgressIndicator() {
        progressIndicator.show()
    }

    private fun dismissProgressIndicator() {
        progressIndicator.hide()
    }

    private fun navigateToMovieDetail(movieId: Long) {
        val detailFragment = MovieDetailFragment.newInstance(movieId)
        fragmentManager?.replaceFragment(detailFragment)
    }

    companion object {
        @JvmStatic
        fun newInstance() = MovieListFragment()
    }
}