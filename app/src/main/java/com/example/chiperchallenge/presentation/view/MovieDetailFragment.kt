package com.example.chiperchallenge.presentation.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.chiperchallenge.R
import com.example.chiperchallenge.di.ViewModelFactory
import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.extensions.onChange
import com.example.chiperchallenge.presentation.viewmodel.MovieDetailViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_detail.*

class MovieDetailFragment : Fragment(R.layout.fragment_movie_detail) {

    private lateinit var viewModel: MovieDetailViewModel
    private val picasso = Picasso.get()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        findMovieIdFromArguments()
        retrieveMovieDetails()
    }

    private fun populateMovie(movie: Movie) {
        voteAverage.text = "Rate ${movie.voteAverage}"
        popularity.text = "Avg ${movie.popularity}"
        movieDescription.text = movie.overview
        loadImage(movie.poster)
    }

    private fun loadImage(poster: String?) {
        picasso.load(poster)
            .into(moviePoster)
    }

    private fun retrieveMovieDetails() {
        viewModel.findMovieBy(findMovieIdFromArguments())
    }

    private fun findMovieIdFromArguments(): Long {
        return arguments?.getLong(MOVIE_ID_KEY) ?: 0L
    }

    private fun initViewModel() {
        viewModel = ViewModelFactory.createMovieDetailViewModel(this, requireContext())
        observeMovieUpdates()
    }

    private fun observeMovieUpdates() {
        onChange(viewModel.movie) {
            updateMovie(it)
        }
    }

    private fun updateMovie(movie: Movie?) {
        movie?.let {
            (activity as? AppCompatActivity)?.supportActionBar?.title = movie.name
            populateMovie(it)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(movieId: Long) =
            MovieDetailFragment().apply {
                arguments = Bundle().apply {
                    putLong(MOVIE_ID_KEY, movieId)
                }
            }

        private const val MOVIE_ID_KEY = "movie_id_key"
    }
}