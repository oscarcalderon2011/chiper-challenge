package com.example.chiperchallenge.presentation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.chiperchallenge.R
import com.example.chiperchallenge.extensions.replaceFragment

class MoviesActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        val moviesListFragment = MovieListFragment.newInstance()
        supportFragmentManager.replaceFragment(moviesListFragment)
    }
}