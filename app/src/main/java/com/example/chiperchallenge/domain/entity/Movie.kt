package com.example.chiperchallenge.domain.entity

data class Movie(
        val id: Long,
        val name: String,
        val voteAverage: Float = 0.0F,
        val popularity: Float = 0.0F,
        val releaseDate: String,
        val thumbnail: String,
        val poster: String?,
        val overview: String = ""
)