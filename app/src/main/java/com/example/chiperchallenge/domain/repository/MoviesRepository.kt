package com.example.chiperchallenge.domain.repository

import com.example.chiperchallenge.domain.entity.Movie
import io.reactivex.Maybe
import io.reactivex.Single

interface MoviesRepository {
    fun findAll(): Single<List<Movie>>
    fun findById(id: MovieId): Maybe<Movie>
}

typealias MovieId = Long