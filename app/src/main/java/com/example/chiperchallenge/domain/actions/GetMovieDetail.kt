package com.example.chiperchallenge.domain.actions

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MoviesRepository
import io.reactivex.Maybe

class GetMovieDetail(private val moviesRepository: MoviesRepository) {

    operator fun invoke(id: Long): Maybe<Movie> {
        return moviesRepository.findById(id)
    }
}