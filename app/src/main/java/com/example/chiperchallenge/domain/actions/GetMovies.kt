package com.example.chiperchallenge.domain.actions

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MoviesRepository
import io.reactivex.Single

class GetMovies(private val moviesRepository: MoviesRepository) {

    operator fun invoke(): Single<List<Movie>> {
        return moviesRepository.findAll()
    }
}




