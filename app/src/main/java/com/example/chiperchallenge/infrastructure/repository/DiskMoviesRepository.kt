package com.example.chiperchallenge.infrastructure.repository

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MoviesRepository
import com.example.chiperchallenge.infrastructure.storage.LocalStorage
import io.reactivex.Maybe
import io.reactivex.Single

class DiskMoviesRepository(private val moviesStorage: LocalStorage<Movie>): MoviesRepository {

    override fun findAll(): Single<List<Movie>> {
        return Single.just(moviesStorage.finAll())
    }

    override fun findById(id: Long ): Maybe<Movie> {
        val movie = moviesStorage.finAll().firstOrNull { it.id == id }
        return if (movie == null) Maybe.empty() else Maybe.just(movie)
    }

    fun addAll(movies: List<Movie>) {
        moviesStorage.addAll(movies)
    }

}