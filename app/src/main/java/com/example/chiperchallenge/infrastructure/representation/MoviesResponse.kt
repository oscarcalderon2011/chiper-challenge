package com.example.chiperchallenge.infrastructure.representation

import com.google.gson.annotations.SerializedName

class MoviesResponse(@SerializedName("results") val movies: List<MovieResponse>)