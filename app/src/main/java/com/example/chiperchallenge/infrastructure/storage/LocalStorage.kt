package com.example.chiperchallenge.infrastructure.storage

interface LocalStorage<T> {
    fun finAll(): List<T>
    fun addAll(movies: List<T>)
}