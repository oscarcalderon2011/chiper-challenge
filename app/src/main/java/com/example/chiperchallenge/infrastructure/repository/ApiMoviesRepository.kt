package com.example.chiperchallenge.infrastructure.repository

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MovieId
import com.example.chiperchallenge.domain.repository.MoviesRepository
import com.example.chiperchallenge.infrastructure.client.TmdbClient
import com.example.chiperchallenge.infrastructure.representation.MovieResponse
import com.example.chiperchallenge.infrastructure.representation.MoviesResponse
import io.reactivex.Maybe
import io.reactivex.Single

class ApiMoviesRepository(private val client: TmdbClient): MoviesRepository {

    override fun findAll(): Single<List<Movie>> {
        return client.findPopularMovies().map { createMovies(it) }
    }

    override fun findById(id: MovieId): Maybe<Movie> {
        throw NotImplementedError()
    }

    private fun createMovies(moviesResponse: MoviesResponse): List<Movie> {
        return moviesResponse.movies.mapNotNull { createMovieFrom(it) }
    }

    private fun createMovieFrom(movieResponse: MovieResponse): Movie? {
        if (movieResponse.name == null
            || movieResponse.releaseDate == null
            || movieResponse.thumbnail == null
            || movieResponse.poster == null
            || movieResponse.overview == null) return null

        return Movie(
            movieResponse.id,
            movieResponse.name,
            movieResponse.voteAverage,
            movieResponse.popularity,
            movieResponse.releaseDate,
            "$thumbnailUrl${movieResponse.thumbnail}",
            "$posterUrl${movieResponse.poster}",
            movieResponse.overview
        )
    }

    companion object {
        private const val thumbnailUrl = "https://image.tmdb.org/t/p/w200"
        private const val posterUrl = "https://image.tmdb.org/t/p/w300"
    }

}


