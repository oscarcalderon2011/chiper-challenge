package com.example.chiperchallenge.infrastructure.storage

import android.content.SharedPreferences
import com.example.chiperchallenge.domain.entity.Movie
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class SharedPreferencesStorage<T>(private val sharedPreferences: SharedPreferences, private val gson: Gson): LocalStorage<T> {

    override fun finAll(): List<T> {
        return getListFromDisk()
    }

    private fun getListFromDisk(): List<T> {
        val jsonOnDisk = findJsonOnDisk() ?: return emptyList()
        return createListFrom(jsonOnDisk)
    }

    private fun findJsonOnDisk(): String? {
        val rawData = sharedPreferences.getString(STORAGE_KEY, "") ?: return null
        if (rawData.isEmpty()) return null
        return rawData
    }

    private fun createListFrom(jsonString: String?): List<T> {
        val type = object : TypeToken<List<Movie>>() {}.type
        return gson.fromJson(jsonString, type)
    }

    override fun addAll(movies: List<T>) {
        val previousItems = getListFromDisk().toMutableList()
        previousItems.addAll(movies)
        sharedPreferences.edit().putString(STORAGE_KEY, gson.toJson(previousItems)).apply()
    }

    private companion object {
        const val STORAGE_KEY = "storage_key"
    }
}