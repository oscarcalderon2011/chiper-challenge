package com.example.chiperchallenge.infrastructure.client

import com.example.chiperchallenge.infrastructure.representation.MoviesResponse
import io.reactivex.Single
import retrofit2.http.GET

interface TmdbClient {
    @GET("movie/popular")
    fun findPopularMovies(): Single<MoviesResponse>
}