package com.example.chiperchallenge.infrastructure.repository

import com.example.chiperchallenge.domain.entity.Movie
import com.example.chiperchallenge.domain.repository.MovieId
import com.example.chiperchallenge.domain.repository.MoviesRepository
import io.reactivex.Maybe
import io.reactivex.Single

class CachedMoviesRepository(
    private val diskMoviesRepository: DiskMoviesRepository,
    private val apiMoviesRepository: ApiMoviesRepository
) : MoviesRepository {

    override fun findAll(): Single<List<Movie>> {
        return diskMoviesRepository.findAll()
            .flatMap {movies ->
                if (movies.isEmpty()) {
                    apiMoviesRepository.findAll().doOnSuccess { saveMovies(it) }
                }
                else {
                    Single.just(movies)
                }
            }
    }

    private fun saveMovies(it: List<Movie>) {
        diskMoviesRepository.addAll(it)
    }

    override fun findById(id: MovieId): Maybe<Movie> {
        return diskMoviesRepository.findById(id)
    }

}