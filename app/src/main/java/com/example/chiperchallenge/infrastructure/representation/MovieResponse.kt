package com.example.chiperchallenge.infrastructure.representation

import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @SerializedName("id") val id: Long,
    @SerializedName("original_title") val name: String? = null,
    @SerializedName("backdrop_path") val thumbnail: String? = null,
    @SerializedName("poster_path") val poster: String? = null,
    @SerializedName("vote_average") val voteAverage: Float = 0.0F,
    @SerializedName("popularity") val popularity: Float = 0.0F,
    @SerializedName("release_date") val releaseDate: String? = null,
    @SerializedName("overview") val overview: String? = null
)