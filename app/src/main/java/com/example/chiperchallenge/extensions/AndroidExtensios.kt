package com.example.chiperchallenge.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.chiperchallenge.R

fun FragmentManager.replaceFragment(fragment: Fragment) {
    addFragmentToActivity(
        this,
        fragment,
        R.id.fragment_container,
        true
    )
}

fun addFragmentToActivity(
    fragmentManager: FragmentManager,
    fragment: Fragment,
    frameId: Int,
    animate: Boolean
) {
    val ft = fragmentManager.beginTransaction()
    if (animate) {
        ft.setCustomAnimations(R.anim.right_in, R.anim.left_out)
    }
    ft.replace(frameId, fragment, fragment.javaClass.toString())
    ft.commitAllowingStateLoss()
}