package com.example.chiperchallenge.extensions

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData

fun <T> Fragment.onChange(liveData: LiveData<T>, action: (T) -> Unit) {
    liveData.observe(this, { action.invoke(it) })
}