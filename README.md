# movies app

La aplicación fue construida siguiendo los principios básicos de interaction driven design y clean architecture en toda la base del diseño del domino del negocio, así como el uso de TDD para el diseño de los casos de uso y sus implementaciones. Para las capas de presentación y de vistas se usó MVVM, con una mezcla de LiveData y RxJava(pendiente migración a corutinas).
Se implemento un sistema en cache con almacenamiento local, que permite no ir a la red cuando las peliculas ya han sido almacenadas. (pendiente migración a algún sistema de base de datos)

Las capas de la aplicación se dividen en los siguientes conceptos:

* **core**: Es donde se encuentran
* **domain**: Es donde se encuentran los conceptos puramente concernientes a la actividad del negocio y los puntos de acceso a los casos de uso de la app.
* **infrastructure**: Es donde se encuentran las implementaciones reales de los conceptos de dominio, por ejemplo implementación de los repositorios o servicios que necesitan tener acceso al sistema o la plataforma.
* **di** Es desde donde se construyen las dependencias para estas implementaciones(pendiente migración a algún inyector de dependencias).
* **presentation**: Es donde se encuentra todo lo relacionado a la capa de vistas de Android, sus viewmodels, y sus elementos de presentación en pantalla.

![alt text](https://gitlab.com/oscarcalderon2011/chiper-challenge/blob/master/Screenshot_20201113-104417.jpg?raw=true)
![alt text](https://gitlab.com/oscarcalderon2011/chiper-challenge/blob/master/Screenshot_20201113-104423.jpg?raw=true)